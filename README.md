# Geekseat - Technical Test - Rian Septiana

## How-To notes

1. Experimented in local K8s using minikube. i created 3 namespaces for splitting dev, staging and production environment

2. Then bootstrap kustomization.yaml which pointing to resources located in initialization directory
  - namespaces.yaml
  - secrets.yaml
  - pvc-gstest.yaml
  - mysql-gstest.yaml
  - wordpress-gstest.yaml

3. Securing developer access to only development namespace by creating new kubeconfig:
  - create service account for developer in dev-access.yaml
    - kubectl create -f access/dev-access.yaml
  - get token : `kubectl get secret developer-user-token-xxxxx -n development -o "jsonpath={.data.token}" | base64 -D`
  - get cert : `kubectl get secret developer-token-9cd8m -n development -o "jsonpath={.data['ca\.crt']}"`
  - edit kubeconfig with token and cert above

4. Using gitlab as Revision control deployed on K8s

  - kubectl apply -f gitlab-k8s-integration/gitlab.yaml
  - Image registry is located outside cluster as i applying the K8s locally. its on simple docker container

5. Deploy gitlab-runner on kubernetes using helm chart with custom values.yaml:

  - helm repo add gitlab https://charts.gitlab.io
  - helm repo update
  - update values.yaml with gitlab url and token for runner
  - helm install --name gitlab-runner -f gitlab-runner/values.yaml gitlab/gitlab-runner

6. Integrate gitlab with K8s
  - on gitlab > admin area > Setting > Network. tick allow requests on Outbound request
  - create service account for gitlab with cluster-admin role :
    - kubectl apply -f gitlab-k8s-integration/gitlab-svc-account.yaml
  - on gitlab > admin area > kubernetes, connect to existing cluster. fill the boxes :
    - API URL
      - `kubectl cluster-info | grep -E 'Kubernetes master|Kubernetes control plane' | awk '/http/ {print $NF}'`
    - CA Certificate
      - `kubectl get secret default-token-xxxxx -o jsonpath="{['data']['ca\.crt']}" | base64 --decode`
    - Service Token
      - `kubectl describe secret gitlab-token-xxxxx -n kube-system `
    - Add kubernetes cluster
    - Go to Created cluster > Application. Install Ingress plugin
7. i setup App from Golang to test CI/CD with .gitlab-ci.yaml. i created a simple 4 stages. test , build, release, deploy. This is the project : [geekseat-test-app](https://gitlab.com/rvnz45/geekseat-test-app)


Thank you for the opportunity for this Technical Test! i gained a great picture on how to setup K8s securely and maintanable !

Sincerely,
Rian Septiana
